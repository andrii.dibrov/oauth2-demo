package com.spd.oauth2demo.web;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class EndpointsController {

    private final OAuth2AuthorizedClientService authorizedClientService;

    public EndpointsController(OAuth2AuthorizedClientService authorizedClientService) {
        this.authorizedClientService = authorizedClientService;
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public String index(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();

//        if(principal instanceof User){
//            User user = (User) principal;
//            model.addAttribute("isLoggedIn", Boolean.TRUE);
//            model.addAttribute("username", "INMEMORY USER "+user.getUsername());
//        }
//        else

        if (principal instanceof DefaultOAuth2User) {
            DefaultOAuth2User user = (DefaultOAuth2User) principal;
            model.addAttribute("isLoggedIn", Boolean.TRUE);
            model.addAttribute("username", user.getAttribute("name"));
        } else {
            model.addAttribute("isLoggedIn", Boolean.FALSE);
            model.addAttribute("username", "Anonymous user");
        }

        return "index";
    }

    @GetMapping("/loginInfo")
    public String getLoginInfo(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        DefaultOAuth2User principal = (DefaultOAuth2User) authentication.getPrincipal();

        List<String> loginInfo = new ArrayList<>();
        Map attributes = principal.getAttributes();
        attributes.forEach((k, v) -> loginInfo.add(k + ": " + v));

        model.addAttribute("loginInfo", loginInfo);
        return "login_info";
    }

    @GetMapping("/loginInfoWithToken")
    public String getLoginInfoWithToken(OAuth2AuthenticationToken authentication, Model model) {

        OAuth2AuthorizedClient client = authorizedClientService
                .loadAuthorizedClient(
                        authentication.getAuthorizedClientRegistrationId(),
                        authentication.getName());

        String userInfoEndpointUri = client
                .getClientRegistration()
                .getProviderDetails()
                .getUserInfoEndpoint()
                .getUri();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken().getTokenValue());
        HttpEntity entity = new HttpEntity("", headers);
        var response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);

        List<String> loginInfo = new ArrayList<>();
        Map attributes = response.getBody();
        attributes.forEach((k, v) -> loginInfo.add(k + ": " + v));

        model.addAttribute("loginInfo", loginInfo);

        return "login_info";
    }
}